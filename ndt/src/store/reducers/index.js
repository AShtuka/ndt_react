import { combineReducers } from 'redux'
import { landingPageReducer } from './landingPage'


export const rootReducer = combineReducers({
    landingPage: landingPageReducer,
});