import React from 'react';

class Registration extends React.Component {
    render() {
        return (
            <div className="container-fluid">
                <div className="row min-vh-100 justify-content-center align-items-center">
                    <div className="col-12 col-sm-10 col-md-8 col-lg-6 col-xl-5">
                        <form>
                            <div className="form-group row">
                                <label htmlFor="name" className="col-sm-3 col-form-label">Name</label>
                                <div className="col-sm-9">
                                    <input type="text" className="form-control" id="name" placeholder="Name"/>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label htmlFor="surname" className="col-sm-3 col-form-label">Surname</label>
                                <div className="col-sm-9">
                                    <input type="text" className="form-control" id="surname" placeholder="Surname"/>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label htmlFor="login" className="col-sm-3 col-form-label">Login</label>
                                <div className="col-sm-9">
                                    <input type="text" className="form-control" id="login" placeholder="Login"/>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label htmlFor="password" className="col-sm-3 col-form-label">Password</label>
                                <div className="col-sm-9">
                                    <input type="password" className="form-control" id="password" placeholder="Password"/>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label htmlFor="passwordCheck" className="col-sm-3 col-form-label">Password</label>
                                <div className="col-sm-9">
                                    <input type="password" className="form-control" id="passwordCheck" placeholder="Repeat password"/>
                                </div>
                            </div>
                            <div className="form-group row">
                                <div className="col-sm-3">NDT Methods</div>
                                <div className="col-sm-2">
                                    <div className="form-check mb-4 mt-1">
                                        <input className="form-check-input" type="checkbox" id="gridCheck1"/>
                                        <label className="form-check-label" htmlFor="gridCheck1">DPI</label>
                                    </div>
                                    <div className="form-check mb-4">
                                        <input className="form-check-input" type="checkbox" id="gridCheck1"/>
                                        <label className="form-check-label" htmlFor="gridCheck1">MPI</label>
                                    </div>
                                    <div className="form-check mb-4">
                                        <input className="form-check-input" type="checkbox" id="gridCheck1"/>
                                        <label className="form-check-label" htmlFor="gridCheck1">ECI</label>
                                    </div>
                                    <div className="form-check">
                                        <input className="form-check-input" type="checkbox" id="gridCheck1"/>
                                        <label className="form-check-label" htmlFor="gridCheck1">USI</label>
                                    </div>
                                </div>
                                <div className="col-sm-7">
                                    <div className="form-group row">
                                        <label htmlFor="DPI" className="col-sm-5 col-form-label col-form-label-sm">DPI valid to</label>
                                        <div className="col-sm-7">
                                            <input type="date" className="form-control form-control-sm" id="DPI"/>
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label htmlFor="DPI" className="col-sm-5 col-form-label col-form-label-sm">MPI valid to</label>
                                        <div className="col-sm-7">
                                            <input type="date" className="form-control form-control-sm" id="DPI"/>
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label htmlFor="DPI" className="col-sm-5 col-form-label col-form-label-sm">ECI valid to</label>
                                        <div className="col-sm-7">
                                            <input type="date" className="form-control form-control-sm" id="DPI"/>
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label htmlFor="DPI" className="col-sm-5 col-form-label col-form-label-sm">USI valid to</label>
                                        <div className="col-sm-7">
                                            <input type="date" className="form-control form-control-sm" id="DPI"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="form-group row">
                                <div className="col-sm-3"/>
                                <div className="col-sm-9">
                                    <div className="form-group row">
                                        <label htmlFor="visual" className="col-sm-5 col-form-label">Visual test valid to</label>
                                        <div className="col-sm-7">
                                            <input type="date" className="form-control form-control-sm" id="visual"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="form-group row">
                                <div className="col-sm-10">
                                    <button type="submit" className="btn btn-primary">Registration</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default Registration;