import React from 'react';

class CarouselIndicator extends React.Component {
    render() {
        return (
            <ol className="carousel-indicators">
                <li data-target="#carousel" data-slide-to="0" className="active"/>
                <li data-target="#carousel" data-slide-to="1"/>
                <li data-target="#carousel" data-slide-to="2"/>
                <li data-target="#carousel" data-slide-to="3"/>
                <li data-target="#carousel" data-slide-to="4"/>
            </ol>
        )
    }
}

export default CarouselIndicator;