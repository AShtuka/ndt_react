import React from 'react';
import CarouselCaption from './CarouselCaption'

class CarouselItem extends React.Component {
    render() {
        const { imgSrc, className } = this.props;
        return (
            <div className={className}>
                <img src={imgSrc} className="d-block w-100" alt="..."/>
                <CarouselCaption />
            </div>
        )
    }
}

export default CarouselItem;