import React from 'react';

class CarouselCaption extends React.Component {
    render() {
        return (
            <div className="carousel-caption d-none d-md-block">
                <h5>Second slide label</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </div>
        )
    }
}

export default CarouselCaption;