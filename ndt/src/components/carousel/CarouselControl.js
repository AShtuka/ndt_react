import React from 'react';

class CarouselControl extends React.Component {
    render() {
        return (
            <>
                <a className="carousel-control-prev" href="#carousel" role="button"  data-slide="prev">
                    <span className="carousel-control-prev-icon" aria-hidden="true"/>
                    <span className="sr-only">Previous</span>
                </a>
                <a className="carousel-control-next" href="#carousel" role="button" data-slide="next">
                    <span className="carousel-control-next-icon" aria-hidden="true"/>
                    <span className="sr-only">Next</span>
                </a>
            </>
        )
    }
}

export default CarouselControl;