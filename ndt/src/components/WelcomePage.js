import React from 'react';

class WelcomePage extends React.Component {
    render() {
        return (
            <div className="container-fluid text-center">
                <div className="row min-vh-100 justify-content-center align-items-center">
                    <div className="col-11 col-sm-6 col-md-4 col-lg-4 col-xl-3">
                       <h1 className="caption">NDT</h1>
                        <div className="btn-group" role="group" aria-label="Basic example">
                            <button type="button" className="btn btn-outline-dark">Login</button>
                            <button type="button" className="btn btn-outline-dark">Registration</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default WelcomePage;