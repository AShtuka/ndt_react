import React from 'react';

class Login extends React.Component {
    render() {
        return (
            <div className="container-fluid text-center">
                <div className="row min-vh-100 justify-content-center align-items-center">
                    <div className="col-11 col-sm-6 col-md-4 col-lg-4 col-xl-3">
                        <form>
                            <div className="form-group">
                                <input type="text" className="form-control" placeholder="Enter your login"/>
                            </div>
                            <div className="form-group">
                                <input type="password" className="form-control" placeholder="Password"/>
                            </div>
                            <button type="submit" className="btn btn-primary">LogIn</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default Login;