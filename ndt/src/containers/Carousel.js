import React from 'react';
import DPI from '../img/1.jpg'
import DPI_1 from '../img/2.jpg'
import ECI from '../img/3.jpg'
import ECI_1 from '../img/4.jpg'
import MPI from '../img/5.jpg'
import CarouselIndicator from '../components/carousel/CarouselIndicator'
import CarouselControl from '../components/carousel/CarouselControl'
import CarouselItem from '../components/carousel/CarouselItem'

class Carousel extends React.Component {
    render() {
        return(
            <div className="container-fluid text-center">
                <div className="row min-vh-100 justify-content-center align-items-end">
                    <div className="col-12 col-sm-12 col-md-12 col-lg-9 col-xl-9">
                        <div id="carousel" className="carousel slide" data-ride="carousel">
                            <CarouselIndicator />
                            <div className="carousel-inner">
                                <CarouselItem imgSrc = {DPI} className = 'carousel-item active'/>
                                <CarouselItem imgSrc = {DPI_1} className = 'carousel-item'/>
                                <CarouselItem imgSrc = {ECI_1} className = 'carousel-item'/>
                                <CarouselItem imgSrc = {ECI} className = 'carousel-item'/>
                                <CarouselItem imgSrc = {MPI} className = 'carousel-item'/>
                            </div>
                            <CarouselControl />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Carousel;