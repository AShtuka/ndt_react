import React from 'react';
import { connect } from 'react-redux'
import './App.css';
import WelcomePage from '../components/WelcomePage'
import Registration from '../components/Registration'
import Login from "../components/Login";

class App extends React.Component {
  render() {
      const { pageToShow } = this.props;
    return (
            <>{
                pageToShow.show === 'WelcomePage' ? <WelcomePage />
                :
                 pageToShow.show === 'Login' ? <Login />
                 : <Registration />
            }</>
        )
  }
}


const mapStateToProps = store => {
    console.log(store);
    return {
        pageToShow: store.landingPage,
    }
}

export default connect(mapStateToProps)(App)
